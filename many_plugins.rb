require 'cinch'

$COUNT = 1

module Cinch
	module Plugin
		module ClassMethods
			def test_hook
				puts self
				hook :pre, :for => [:match], :method => :test_hook
			end
		end

		def test_hook(m)
			m.reply "hook #{$COUNT} executed for #{self.class}"
			$COUNT += 1
			true
		end
	end
end

class TestPlugin01
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin02
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin03
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin04
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin05
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin06
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin07
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin08
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin09
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin10
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin11
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin12
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin13
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin14
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin15
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin16
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin17
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin18
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin19
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin20
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin21
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin22
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin23
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin24
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin25
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin26
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin27
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin28
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin29
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin30
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin31
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin32
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin33
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin34
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin35
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin36
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin37
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin38
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin39
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin40
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin41
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin42
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin43
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin44
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin45
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin46
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin47
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin48
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin49
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin50
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin51
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin52
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin53
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin54
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin55
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin56
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin57
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin58
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin59
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin60
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin61
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin62
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin63
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin64
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin65
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin66
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin67
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin68
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin69
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin70
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin71
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

class TestPlugin72
	include Cinch::Plugin
	test_hook

	def initialize(*args)
		super
		@test = Hash[*[*1..1000]]
	end

	match /test$/, method: :test

	def test(m)
		m.reply self.class
	end
end

Cinch::Bot.new do
	configure do |conf|
		conf.nick = 'plugins_test'
		conf.realname = 'plugins_test'
		conf.user = 'plugins_test'
		conf.server = ARGV[0]
		conf.channels = ARGV[1].split(',')
		conf.port = 6667
		conf.ssl.use = false
		conf.plugins.prefix = /^!/
		conf.plugins.plugins = ("01".."72").map { |n| Object.const_get("TestPlugin#{n}") }
	end
end.start
